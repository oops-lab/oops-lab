class AccessSpecifierDemo {
    private int priVar;
    protected int proVar;
    public int pubVar;

    void setVar(int priValue,int proValue,int pubValue){
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue; 
    }

    void getVar(){
        System.out.println("priVar = " + priVar);
        System.out.println("proVar = " + proVar );
        System.out.println("pubVar = " + pubVar );
    }
    public static void main(String[] args){
        AccessSpecifierDemo obj = new AccessSpecifierDemo();
        obj.setVar(10,20,30);
        obj.getVar();
    }
}