public class Boxing {
    public static void main(String[] args) {
        System.out.println("Boxing");
        Double f = 99.9;
        Integer i = 84;
        System.out.println("f= "+ f);
        System.out.println("i= " + i);

        System.out.println("UnBoxing");
        double f1 = f;
        int i1 = i;
        System.out.println("f1= " + f1);
        System.out.println("i1= " + i1);
    }
}
