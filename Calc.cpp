#include <iostream>
using namespace std;
int main(){
    float n1,n2;
    int f1,f2;
    char op;
    cout << "Enter first number :";
    cin >> n1;
    cout << "Enter second number :";
    cin >> n2;
    cout << "Enter operator :";
    cin >> op;
    switch (op){
        case '+':cout << n1 << " + " << n2 << " = " << n1+n2 << endl;
        break;
        case '-':cout << n1 << " - " << n2 << " = " << n1-n2 << endl;
        break;
        case '*':cout << n1 << " * " << n2 << " = " << n1*n2 << endl;
        break;
        case '/':cout << n1 << " / " << n2 << " = " << n1/n2 << endl;
        break;
        case '%': f1=int(n1);
                  f2=int(n2);
                  cout << n1 << " % " << n2 << " = " << f1%f2 << endl;
        break;
        default : cout << "Invalid operator !" << endl;
        break;
    }
    return 0;
}