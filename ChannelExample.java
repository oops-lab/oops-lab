import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.WritableByteChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class ChannelExample {
    public static void main(String[] args) {
        try {
            // Open a file channel
            Path filePath = Path.of("C:/Users/Sai manikanta/OneDrive/Documents/Abc.txt");
            FileChannel fileChannel = FileChannel.open(filePath, StandardOpenOption.CREATE, StandardOpenOption.WRITE);

            // Check if the channel is open
            if (fileChannel.isOpen()) {
                System.out.println("Channel is open");
            } else {
                System.out.println("Channel is not open");
            }

            // Write data to the channel
            String message = "Hello, Channel!";
            ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());
            int bytesWritten = fileChannel.write(buffer);
            System.out.println("Bytes written: " + bytesWritten);

            // Read data from the channel
            ByteBuffer readBuffer = ByteBuffer.allocate(1024);
            int bytesRead = fileChannel.read(readBuffer);
            readBuffer.flip();
            byte[] data = new byte[bytesRead];
            readBuffer.get(data);
            System.out.println("Read data: " + new String(data));

            // Truncate the size of the channel
            long newSize = 10;
            fileChannel.truncate(newSize);
            System.out.println("New size: " + fileChannel.size());

            
            // Close the channels
            fileChannel.close();

            // Check if the channel is open after closing
            if (fileChannel.isOpen()) {
                System.out.println("Channel is open");
            } else {
                System.out.println("Channel is closed");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
