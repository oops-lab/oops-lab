#include<iostream>
using namespace std;
class Animal{
    public:
    int age=19;
    void walk(){
        cout<< "Animal walks"<<endl;
    }
};
class Tiger:virtual public Animal{
    public:
    Tiger(){
        cout<< "Tiger Walks"<<endl;
    }
};
class Lion:virtual public Animal{
    public:
    Lion(){
        cout<<"Lion walks"<<endl;
    }
};
class Liger:public Lion,public Tiger{
    public:
    Liger(){
        cout<<"Liger walks"<<endl;
    }
};
int main(){
    Liger vd;
    cout<<"Age="<<vd.age<<endl;
    vd.walk();
}