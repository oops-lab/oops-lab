#include<stdio.h>
struct Employee{
    char name[50];
    int id;
    char dept[50];
    char designation[50];
    float salary;
}emp[20];

int main(){
    int n;
    FILE *fp;
    fp= fopen("EmployeeRec.txt","w");

    printf("Number of Employees details to be stored: ");
    scanf("%d",&n);
    for(int i=0;i<n;i++){
        printf("Enter employee %d details:(Name,Id,Dept,Desg,Sal)\n",i+1);
        scanf("%s %d %s %s %f",emp[i].name,&emp[i].id,emp[i].dept,emp[i].designation,&emp[i].salary);
    }
    for(int j=0;j<n;j++){
        fprintf(fp,"Employee %d details:",j+1);
        fprintf(fp,"\n Name: %s\n Id: %d\n Department:%s\n Designation:%s\n Salary:%.2f\n",emp[j].name,emp[j].id,emp[j].dept,emp[j].designation,emp[j].salary);
    }
    fclose(fp);

    char ch;
    FILE *f;
    f = fopen("EmployeeRec.txt", "r");
    if (NULL == f) {
        printf("file can't be opened \n");
    }
    do {
        ch = fgetc(f);
        printf("%c", ch);
    } while (ch != EOF);
    fclose(f);
    return 0;
}