import java.util.Scanner;
class Except{
    public static void main(String[] args) {
        
        try{
            Scanner s = new Scanner(System.in);
            System.out.print("Enter 2 numbers: ");
            int a = s.nextInt();
            int b = s.nextInt();
            int div = a/b;//exception
        }catch(ArithmeticException e){
            System.out.println("Divide by Zero is Not allowed !\n");
        }
        try{
            int arr[] = new int[]{2, 5,7,1};
            System.out.print("Array: ");
            for(int i=0;i<4;i++){
                System.out.print(arr[i] + " ");
            }
            System.out.println(arr[6]);//exeption
            System.out.println(arr[-2]);//exception 
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("\nInvalid Array index !\n");
        }
        try{
            String a= null;
            System.out.println(a.charAt(1));//exception
        }catch(NullPointerException e){
            System.out.println("NULL Pointer !\n");
        }
        try{
            Class.forName("Sai.class");//exception
        }catch(ClassNotFoundException e){
            System.out.println("Given class is not found !\n");
        }
        try{
            Thread t = new Thread();
            t.sleep(1000);
        }catch(InterruptedException e){
            System.out.println("Interrupted Exception !");
        }
    }
}