import java.io.*;
class FileCopy {
    public static void main(String[] args) {
       
        File inpf = new File("C:/Users/Sai manikanta/OneDrive/Documents/Abc.txt"); 
        File outf = new File("C:/Users/Sai manikanta/OneDrive/Documents/Xyz.txt");
        try{
            FileInputStream in = new FileInputStream(inpf);
            FileOutputStream out = new FileOutputStream(outf);

            int data;
            while((data=in.read())!=-1){
                out.write(data);
            }
            System.out.println("File Copy Successful");
            in.close();
            out.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("File not found" );
        } catch (IOException e) {
            System.out.println("Error copying file ");
        }catch (NullPointerException e) {
            System.out.println("One of the file paths is null");
        }
    }
}
