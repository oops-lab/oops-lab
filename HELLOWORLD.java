import java.applet.Applet;
import java.awt.Graphics;
 
/*
<applet code="HelloWorld" width=200 height=60>
</applet>
*/
 
public class HelloWorld extends Applet
{
    public void paint(Graphics g)
    {
        g.drawString("Hello World", 20, 20);
    }
     
}
