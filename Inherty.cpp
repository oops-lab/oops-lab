#include<iostream>
using namespace std;
//hybrid
class Flour{
    public:
    int f=2;
    int getflour(){
        return f;
    }
};
class Sugar{
   public:
    int s=1;
    int getsugar(){
        return s;
    }
};
//multiple
class Mixer:public Flour,public Sugar{
    public:
    void mixed(){
        cout<<"Mix the content "<< endl;
    }
};
//hierarchial
class Essence:public Mixer{
    public:
    string flavour= "Vanilla";
    string getflavour(){
        return flavour;
    }
};
class Batter:public Mixer{
    public:
    void getbatter(){
        cout<< "Make smooth mixed cake Batter"<<endl;
    }
};
//multilevel
class Oven:public Batter{
    public:
    void Bake(){
        cout<< "Bake the cake"<< endl ;
    }
};
class Cake:public Oven{
    public:
    void cake(){
        cout<<"Cake is Ready"<< endl;
    }
};
int main(){
    Mixer mix;
    Essence vanilla;
    Batter batter;
    Cake plumcake;
    cout << "Add " << mix.getflour() << "kg Of maida"<<endl;
    cout<< "Add "<< mix.getsugar() << "kg of Sugar"<< endl;
    mix.mixed();
    cout << "Add "<< vanilla.getflavour()<< " flavour"<<endl;
    batter.getbatter();
    plumcake.Bake();
    plumcake.cake();
return 0;
}