#include<iostream>
using namespace std;
class Account{
    public:
    float balance=0;
    void deposit(int amount){
        balance = balance + amount;
        cout << "Rs." << amount << " Deposited" << endl;
    }
    void deposit(double amount){
        balance = balance + amount;
        cout << "Rs." << amount << " Deposited" << endl;
    }
    void withdraw(int amount){
        balance = balance - amount;
        cout << "Rs." << amount << " Withdrawn" << endl;
    }
    void withdraw(double amount){
        balance = balance - amount;
        cout << "Rs." << amount << " Withdrawn" << endl;
    }
    void accBalance(){
        cout<< "Balance : "<<balance << endl;
    }
};
int main(){
    Account a ;
    a.accBalance();
    a.deposit(100000);
    a.withdraw(5000);
    a.deposit(100.5);
    a.withdraw(55.55);
    a.accBalance();
}