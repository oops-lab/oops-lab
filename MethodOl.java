class Bank {
    void transaction(String accountType,double amount) {
        System.out.println("Performing a " + accountType + " transaction of amount " + amount);
    }

    void transaction(String accountType, double amount,  String transactionType) {
        System.out.println("Performing a " + transactionType + " transaction of amount " + amount + " on " + accountType);
    }

    void transaction(String accountType,double amount, String transactionType, String date) {
        System.out.println("Performing a " + transactionType + " transaction of amount " + amount + " on " + accountType + " on " + date);
    }
}
class MethodOl{
    public static void main(String[] args) {
        Bank acc = new Bank();
        acc.transaction("Savings Account",10000);
        acc.transaction("Current Account",20000.15 , "deposit");
        acc.transaction("Personal Account", 50000.5, "withdraw", "20-04-2023");
    }
}
