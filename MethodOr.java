class BankAccount {
    double balance=100000;
    BankAccount(){
        System.out.println("Account Balance: " + balance);
    }
    void withdraw(double amount) {
        balance = balance - amount;
    }
}
class SavingsAccount extends BankAccount {
    int withdrawals = 0;
    void withdraw(double amount) {
        if (withdrawals < 3) { // Limiting 3 withdrawals per month
            balance -= amount;
            withdrawals++;
            System.out.println("Savings Account Balance after withdrawl : "+ balance);
        } else {
            System.out.println("You have exceeded the number of withdrawals allowed for this month");
        }
    }
}
class CurrentAccount extends BankAccount {
    int withdrawals = 0;
    void withdraw(double amount) {
        if (withdrawals < 2) { // Limiting 2 withdrawals per month
            balance -= amount;
            withdrawals++;
            System.out.println("Current Account Balance after withdrawl : "+ balance);
        } else {
            System.out.println("You have exceeded the number of withdrawals allowed for this month");
        }
    }
}
class MethodOr{
    public static void main(String[] args) {
        SavingsAccount sc = new SavingsAccount();
        System.out.println("Savings Account");
        sc.withdraw(10000);
        sc.withdraw(20000);
        sc.withdraw(30000);

        System.out.println("\nCurrent Account");
        CurrentAccount cc = new CurrentAccount();
        cc.withdraw(50000);
        cc.withdraw(2000.5);
        cc.withdraw(15000);
    }
}