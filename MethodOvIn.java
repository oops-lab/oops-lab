class Account {
    void transaction(String type, double amount, String date) {
        System.out.println("Performed a " + type + " transaction of amount " + amount + " on " + date);
    }
}
class SavingsAccount extends Account {
    void transaction(double amount, String date) {
        super.transaction("Savings Deposit", amount, date);
    }
}

class CurrentAccount extends Account {
    void transaction(double amount, String date) {
        super.transaction("Current Withdrawal", amount, date);
    }
}

class MethodOvIn{
    public static void main(String[] args) {
        SavingsAccount s = new SavingsAccount();
        s.transaction(50000.0, "20-04-2023");
        CurrentAccount c= new CurrentAccount();
        c.transaction(10000.0,"21-04-2023");
    }
}
