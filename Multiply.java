import java.util.Scanner;
class Multiply extends Thread{
    void multiplication(int n){
        for (int i =1; i<=10;i++){
            System.out.println(n + " * "+ i +" = " + n*i);
            try{
                Thread.sleep(1000);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }
    public void run(){
        int n;
        Scanner s = new Scanner(System.in);
        System.out.println("Enter a Number:");
        n= s.nextInt();
        multiplication(n);
    }
    public static void main(String[] args) {
        Multiply obj = new Multiply();
        Multiply obj1 = new Multiply();
        obj.start();
        obj1.start();
    }
}

