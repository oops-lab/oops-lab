import java.util.Scanner;
class MultiplyRun implements Runnable{
    void multiplication(int n){
        for (int i =1; i<=10;i++){
            System.out.println(n + " * "+ i +" = " + n*i);
            try{
                Thread.sleep(1000);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }
    public void run(){
        int n;
        Scanner s = new Scanner(System.in);
        System.out.println("Enter a number: ");
        n=s.nextInt();
        multiplication(n);
    }

    public static void main(String[] args) {
        MultiplyRun obj = new MultiplyRun(); 
        MultiplyRun obj1 = new MultiplyRun();
        obj.run();
        obj1.run();
    }
}