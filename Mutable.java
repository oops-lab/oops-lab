import java.util.ArrayList;

class Mutable {
    public static void main(String[] args) {
        boolean b = true;
        double d = 84.83;
        String s = "ABRAKADABRA";

        System.out.println("Boolean before : " + b);
        System.out.println("Id:"+ System.identityHashCode(b));
        b=false;
        System.out.println("Boolean after : "+b);
        System.out.println("Id:"+ System.identityHashCode(b));

        System.out.println("\nDouble before : " + d);
        System.out.println("Id:"+ System.identityHashCode(d));
        d = 83.84;
        System.out.println("Double after : "+ d);
        System.out.println("Id:"+ System.identityHashCode(d));

        System.out.println("\nString  before : " + s);
        System.out.println("Id:"+ System.identityHashCode(s));
        s = "ABRAKA";
        System.out.println("String after : "+ s);
        System.out.println("Id:"+ System.identityHashCode(s));


        StringBuilder sb = new StringBuilder("Sai");
        System.out.println("\n\nStringBuilder  before : " + sb);
        System.out.println("Id:"+ System.identityHashCode(sb));
        sb.insert(3," Manikanta");
        System.out.println("StringBuilder after : "+ sb);
        System.out.println("Id:"+ System.identityHashCode(sb));

        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(83);
        arr.add(84);
        arr.add(85);
        System.out.println("\nArrayList  before : " + arr);
        System.out.println("Id:"+ System.identityHashCode(arr));
        arr.remove(2);
        System.out.println("ArrayList after : "+ arr);
        System.out.println("Id:"+ System.identityHashCode(arr));
    }    
}
