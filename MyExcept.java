import java.util.*;
class MinimumbalanceException extends Exception {
    MinimumbalanceException(String msg) {
        super(msg);
    } 
}

class MyExcept{
    void withdraw(int n) throws MinimumbalanceException{
        if(n<=1000) { 
            throw new MinimumbalanceException("Withdraw cannot possible!");
        }else{
            System.out.println("Withdrawl successfull");
        }
    }
	public static void main(String[] args) throws MinimumbalanceException {
	    MyExcept obj = new MyExcept();
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter amount:");
	    int withdrawAmount = sc.nextInt();
        obj.withdraw(withdrawAmount);
	}
}