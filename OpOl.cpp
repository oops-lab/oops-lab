#include<iostream>
using namespace std;
class Complex{
    private:
    int real,imag;
    public:
    Complex(int real=0,int imag=0){
        this->real=real;
        this->imag=imag;
    }
    Complex operator +(Complex obj){
        Complex r;
        r.real=real+obj.real;
        r.imag= imag + obj.imag;
        return r;
    }
    Complex operator -(Complex obj){
        Complex r;
        r.real=real-obj.real;
        r.imag= imag - obj.imag;
        return r;
    }
    void result(){
        cout<<real<<" +i"<<imag<<endl;
    }
};
int main(){
    Complex x(20,15),y(15,10);
    Complex a = x + y;
    Complex b = x - y;
    cout << "After Addition : "; 
    a.result();
    cout << "After Subtraction : ";
    b.result();
}