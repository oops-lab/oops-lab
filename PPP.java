// Parent class
class Animal {
  private String name;

  public Animal(String name) {
    this.name = name;
    System.out.println("Animal Called");
  }

  public void eat() {
    System.out.println(name + " is eating.");
  }

  protected void sleep() {
    System.out.println(name + " is sleeping.");
  }

  private void breathe() {
    System.out.println(name + " is breathing.");
  }
}

// Child class
class Dog extends Animal {
  public Dog(String name) {
    super(name);
  }
}
class Ppp{
  public static void main(String[] args) {
    Dog myDog = new Dog("Mani");
    myDog.eat();
    myDog.sleep();
    //myDog.breathe();//can't Access 
}
}