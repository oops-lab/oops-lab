#include <iostream>
using namespace std;
class Account{
  public:
  double balance=100000;
  void accBalance(){
    cout << "Balance = " << balance << endl;
  }
  void deposit(double amount) {
    balance += amount;
  }
  virtual double calculateInterest() = 0;
};

class SavingsAccount : public Account {
public:
  double calculateInterest() {
    double interest_rate = 0.02;
    return balance * interest_rate;
  }
};

class CurrentAccount : public Account {
public:
  double calculateInterest() {
    double interest_rate = 0.01;
    return balance * interest_rate;
  }
};

int main() {
  SavingsAccount sc;
  cout << "Savings Account:"<< endl;
  sc.accBalance();
  sc.deposit(500.0);
  sc.accBalance();
  cout<<  "Interest is " << sc.calculateInterest() << endl << "" << endl;

  CurrentAccount cc;
  cout << "Current Account:"<< endl;
  cc.accBalance();
  cc.deposit(10000);
  cc.accBalance();
  cout << "Interest is " << cc.calculateInterest() << endl;
  return 0;
}