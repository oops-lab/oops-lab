abstract class Account{
    double balance=100000;
    void accBalance(){
        System.out.println("Balance = " + balance );
    }
    void deposit(double amount) {
        balance += amount;
    }
    abstract double calculateInterest();
}

class SavingsAccount extends Account {
    double calculateInterest() {
        double interest_rate = 0.02;
        return balance * interest_rate;
    }
}
class CurrentAccount extends Account {
    double calculateInterest() {
        double interest_rate = 0.01;
        return balance * interest_rate;
    }
}
class ParAbs{
    public static void main(String[] args) {
        SavingsAccount sc = new SavingsAccount();
        System.out.println("Savings Account:");
        sc.accBalance();
        sc.deposit(500.0);
        sc.accBalance();
        System.out.println("Interest is " + sc.calculateInterest());

        CurrentAccount cc = new CurrentAccount();
        System.out.println("\nCurrent Account:");
        cc.accBalance();
        cc.deposit(10000);
        cc.accBalance();
        System.out.println("Interest is " + cc.calculateInterest());
    }
}
