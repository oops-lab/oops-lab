#include<iostream>
using namespace std;
 //pure abstract class
class Account{
    public:
    virtual void interest()=0;
};
class SavingsAcc: public Account{
    public:
    double balance;
    void interest(){
        double interest = balance * 0.01;
        balance = balance + interest;
        cout<< "The balance after crediting interest is : " << balance << endl;
    }
};
class LoanAcc: public Account{
    public:
    double balance;
    void interest(){
        double interest = balance * 0.01;
        balance = balance + interest;
        cout<< "The balance after crediting interest is : " << balance << endl;
    }
};
int main(){
    SavingsAcc acc;
    cout<< "Savings Account" << endl; 
    acc.balance= 10000;
    cout<< "Account Balance : " << acc.balance << endl;
    acc.interest();
    cout << " " << endl;

    LoanAcc lcc;
    cout << "Loan Account " << endl;
    lcc.balance = 20000;
    cout << "Account Balance : " << lcc.balance << endl;
    lcc.interest();
}