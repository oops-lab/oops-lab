interface Account {
    void balance();
    void interest();
}
class SavingsAcc implements Account{
    double bal=10000.0;
    public void balance(){
        System.out.println("Savings Bank Balance : " + bal);
    }
    public void interest(){
        double interest = bal * 0.01;
        bal = bal + interest;
        System.out.println("The balance after crediting interest is : " +bal);
    }
}
class CurrentAcc implements Account{
    double bal=50000.0;
    public void balance(){
        System.out.println("Current Bank Balance : " + bal);
    }
    public void interest(){
        double interest = bal * 0.05;
        bal = bal + interest;
        System.out.println("The balance after crediting interest is : " +bal);
    }
}
class PureAbs{
    public static void main(String[] args) {
        SavingsAcc acc= new SavingsAcc();
        acc.balance();
        acc.interest();
        CurrentAcc cc= new CurrentAcc();
        cc.balance();
        cc.interest();
    }
}