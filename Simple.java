class Brand {
    String brandName;
    Brand(String name) {
        brandName = name;
    }
}
class Mobile extends Brand {
    String modelName;
    Mobile(String brand, String model) {
        super(brand); // Call the constructor of the parent class with the brand name
        modelName = model;
    }
    void showMobile() {
        System.out.println("Brand Name: " + brandName);
        System.out.println("Model Name: " + modelName);
    }
}

public class Simple {
    public static void main(String[] args) {
        Mobile m = new Mobile("ONE PLUS", "NORD CE2");
        m.showMobile();
    }
}
