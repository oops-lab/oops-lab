import java.lang.*;
class Student {
    private String fullName;
    private double semPercentage;
    private String collegeName;
    private int collegeCode;

    // default constructor
    public Student(){
        collegeName = "MVGR";
        collegeCode = 33;
        System.out.println("Default Constructor called !");
        System.out.println("College Name :"+collegeName);
        System.out.println("College Code :"+collegeCode);
    }

    // parameterized constructor
    public Student(String fName, double percentage) {
        fullName=fName ;
        semPercentage=percentage;
        System.out.println("\nParameterized constructor called !");
        System.out.println("Full name :" + fullName);
        System.out.println("Sem Percentage :"+semPercentage);
    }

    // destructor
     protected void finalize() {
        System.out.println("destroyed !");
    }
   
    public static void main(String[] args) {
        Student m = new Student();
        Student s = new Student("Sai",95.6);
        m=null;
        s=null;
        System.gc();
    }
}
