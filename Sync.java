import java.util.Scanner;
class Sync implements Runnable{
    synchronized void multipliction(int n){
        for (int i =1; i<=10;i++){
            System.out.println(n + " * "+ i +" = " + n*i);
            try{
                Thread.sleep(1000);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }
    public void run(){
        int n;
        Scanner s = new Scanner(System.in);
        System.out.println("Enter a number: ");
        n=s.nextInt();
        multipliction(n);
    }

    public static void main(String[] args) {
        Sync obj = new Sync(); 
        Sync obj1 = new Sync();
        obj.run();
        obj1.run();
    }
}