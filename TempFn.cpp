#include<iostream>
using namespace std;
template<class Calculate>
class Template {
    public:
    void add(Calculate n1,Calculate n2) {
        cout<<"Addition is : "<< n1 + n2 <<endl;
    }
    void dec(Calculate n1) {
        cout<<"Decrement is " << --n1 <<endl;
    }
};
int main() {
    Template<float>obj;
    obj.add(1.5,2.99);
    obj.dec(84);
    obj.add(19,20);
    obj.dec(100);
}
