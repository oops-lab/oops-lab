class Chairman {
    String chairman="Dr.P.Ashok Gajapathi Raju";
}
//hierarchical
class Principal extends Chairman {
    String principal="Dr.K V L Raju";
}
class Hod extends Chairman {
    String hod="Dr.P.Ravi Kiran Varma";
}
//single and multilevel
class Faculty extends Hod {
    String fac="M.Vamsi Krishna";
}
class Student extends Faculty {
    String stu= "Sai Manikanta";
}
//multiple Inheritance is not possible

class TypeInher{
    public static void main(String[] args) {
    Student s = new Student();
    Faculty f = new Faculty();
    Hod h = new Hod();
    Principal p = new Principal();
    System.out.println("MVGR COLLEGE OF ENGINEERING");
    System.out.println("ChairMan : " +h.chairman);
    System.out.println("Principal : " + p.principal);
    System.out.println("Hod : " + f.hod);
    System.out.println("Faculty : " +s.fac);
    System.out.println("Student : " +s.stu);
    }
}
