#include <iostream>
#include <iomanip> 

using namespace std;

int main() {
    // endl and flush
    cout << "MVGR College of engineering" << endl;
    cout.flush();

    //  ws
    string name;
    cout << "Enter a name: ";
    cin >> ws >> name;
    cout << "Hello " << name << endl;

    // ends 
    string str1 = "Welcome ", str2 = "To MVGR !";
    cout << str1 << ends << str2 << endl;

    // setw and setfill
    cout << setw(10) << setfill('*') << "Hi" << endl;
    cout << setw(10) << setfill('*') << "Hello" << endl;

    // setprecision manipulator
    double percentage = 93.14159265359;
    cout << "Percentage : " << setprecision(4) << percentage << endl;
    return 0;
}
