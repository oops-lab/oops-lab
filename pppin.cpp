#include<iostream>
using namespace std;
class Base{
    public:
        int pub=10;
    protected:
        int pro=20;
    private:
        int pvt=30;
    public:
    int getpvt(){
        return pvt;
    }
};
class PublicDer:public Base{
    public:
    int getpro(){
        return pro;
    }
};
class ProtectDer:protected Base{
    public:
    int getpub(){
        return pub;
    }
    int getpro(){
        return pro;
    }
};
class PrivateDer:private Base{
    public:
    int getpub(){
        return pub;
    }
    int getpro(){
        return pro;
    }
};
int main(){
    PublicDer obj1;
    ProtectDer obj2;
    PrivateDer obj3;
    cout << "Public Mode:"<<endl;
    cout<<"pub="<< obj1.pub<<endl;
    cout<<"pro="<< obj1.getpro()<<endl;
    cout<<"pvt="<< obj1.getpvt()<<endl;

    cout<<"Protected Mode:"<<endl;
    cout<<"pub="<< obj2.getpub()<<endl;
    cout<<"pro="<< obj2.getpro()<<endl;
    cout<<"pvt="<< "Not Accessible"<<endl;

    cout<< "Private Mode:"<<endl;
    cout<<"pub="<< obj3.getpub()<<endl;
    cout<<"pro="<< obj3.getpro()<<endl;
    cout<<"pvt="<< "Not Accessible" <<endl;
}